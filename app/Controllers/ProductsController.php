<?php
/**
 * Created by PhpStorm.
 * User: Marcos Rubens de Camargo
 * Date: 27/02/2019
 * Time: 00:16
 *Autor : Marcos Rubens de Camargo https://github.com/MarcosRCamargo
 */

namespace App\Controllers;

use SON\Controller;
use App\Models\Product;


class ProductsController extends Controller
{
    public function __construct(Product $model)
    {
        var_dump(get_class($model));
        $this->model = $model;
    }
    public function index()
    {
        $this->render();
    }
}