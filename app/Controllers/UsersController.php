<?php
/**
 * Created by PhpStorm.
 * User: Marcos Rubens de Camargo
 * Date: 22/02/2019
 * Time: 22:52
 *Autor : Marcos Rubens de Camargo https://github.com/MarcosRCamargo
 */

namespace App\Controllers;


use App\Models\User;
use SON\Controller;

class UsersController extends Controller
{
    public function __construct(User $model)
    {
        var_dump(get_class($model));
        $this->model = $model;
    }
    public function index()
    {
        $user = $this->model->get();
        $this->render($user);
    }
    public function create()
    {
        return "Página de Cadastro";
    }
}