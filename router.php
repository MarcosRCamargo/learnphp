<?php
/**
 * Created by PhpStorm.
 * User: Marcos Rubens de Camargo
 * Date: 22/02/2019
 * Time: 23:00
 *Autor : Marcos Rubens de Camargo https://github.com/MarcosRCamargo
 */
$router = new SON\Router;

$router['/'] = [
    'class' => App\Controllers\UsersController::class,
    'action' => 'index'
];
$router['/create'] = [
    'class' => App\Controllers\UsersController::class,
    'action' => 'create'
];
$router['/products'] = [
    'class' => App\Controllers\ProductsController::class,
    'action' => 'index'
];

return $router;