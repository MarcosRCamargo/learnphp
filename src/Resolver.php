<?php
/**
 * Created by PhpStorm.
 * User: Marcos Rubens de Camargo
 * Date: 25/02/2019
 * Time: 22:04
 *Autor : Marcos Rubens de Camargo https://github.com/MarcosRCamargo
 */

namespace SON;

class Resolver
{
    public function handler(string $class, string $method = null)
    {
        $ref = new \ReflectionClass($class);
        $instance = $this->getInstance($ref);

        if (!$method) {
            return $instance;
        }
        $ref_method = new \ReflectionMethod($instance, $method);
        $parameters = $this->methodResolver($ref, $ref_method);

        call_user_func_array([$instance, $method], $parameters);

    }

    private function getInstance($ref)
    {
        $contructor = $ref->getConstructor();
        if (!$contructor) {
            return $ref->newInstance();
        }
        $parameters = $this->methodResolver($ref, $contructor);
        return $ref->newInstanceArgs($parameters);
    }

    private function methodResolver($ref, $method)
    {
        $parameters = [];

        foreach ($method->getParameters() as $param) {

            if ($param->getClass()) {
                $parameters[] = $this->handler($param->getClass()->getName());
                continue;
            }
        }
        return $parameters; //
    }

}