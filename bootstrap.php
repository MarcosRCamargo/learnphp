<?php
/**
 * Created by PhpStorm.
 * User: Marcos Rubens de Camargo
 * Date: 22/02/2019
 * Time: 22:51
 *Autor : Marcos Rubens de Camargo https://github.com/MarcosRCamargo
 */

require __DIR__ . '/vendor/autoload.php';
$router = require __DIR__ . '/router.php';

$object = $router->handler();


(new SON\Resolver)->handler($object['class'], $object['action']);